package br.edu.up.dominio;

import java.io.Serializable;
import java.util.Random;

import javax.xml.bind.annotation.XmlType;

@XmlType
public class Endereco implements Serializable {

	private static final long serialVersionUID = -4456167537794137749L;
	private Integer id;
	private String rua;
	private String numero;
	
	public Endereco() {
	}

	public Endereco(String rua, String numero) {
		this.id = new Random().nextInt(10000); 
		this.rua = rua;
		this.numero = numero;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}