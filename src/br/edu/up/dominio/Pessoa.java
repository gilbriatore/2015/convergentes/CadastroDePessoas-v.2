package br.edu.up.dominio;

import java.io.Serializable;
import java.util.Random;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 2272559069343245287L;
	private Integer id;
	private String nome;
	private byte[] foto;
	private Endereco endereco;

	public Pessoa() {
	}

	public Pessoa(String nome, byte[] foto, Endereco endereco) {
		this.id = new Random().nextInt(10000);
		this.nome = nome;
		this.foto = foto;
		this.endereco = endereco;
	}
	
	public Pessoa(Integer id, String nome, byte[] foto, Endereco endereco) {
		this.id = id;
		this.nome = nome;
		this.foto = foto;
		this.endereco = endereco;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}