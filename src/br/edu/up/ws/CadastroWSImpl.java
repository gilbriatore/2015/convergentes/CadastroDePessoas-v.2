package br.edu.up.ws;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import br.edu.up.dominio.Cadastro;
import br.edu.up.dominio.Pessoa;

@WebService(endpointInterface="br.edu.up.ws.CadastroWS")
public class CadastroWSImpl implements CadastroWS {
	
	@Resource
	private WebServiceContext wsctx;

	@Override
	public void salvar(Pessoa pessoa) {
		CRUD().put(pessoa.getId(), pessoa);
	}

	@Override
	public void excluir(Integer idPessoa) {
		CRUD().remove(idPessoa);
	}

	@Override
	public Pessoa buscar(Integer idPessoa) {
		return CRUD().get(idPessoa);
	}

	@Override
	public List<Pessoa> listar() {
		return new ArrayList<Pessoa>(CRUD().values());
	}
	
	private Cadastro CRUD() {
		ServletContext ctx = (ServletContext) wsctx.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		return Cadastro.CRUD(ctx.getRealPath("/WEB-INF/"));
	}
}