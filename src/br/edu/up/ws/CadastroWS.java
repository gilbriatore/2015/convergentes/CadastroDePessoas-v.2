package br.edu.up.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import br.edu.up.dominio.Pessoa;

@WebService
@SOAPBinding(style=Style.DOCUMENT)
public interface CadastroWS {
	
	@WebMethod
	void salvar(Pessoa pessoa);
	
	@WebMethod
	void excluir(Integer idPessoa);
	
	@WebMethod
	Pessoa buscar(Integer idPessoa);
	
	@WebMethod
	List<Pessoa> listar();
}